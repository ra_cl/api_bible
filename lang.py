import configparser
import os

def lang() -> dict:
    lang = configparser.ConfigParser()
    lang.read('lang.ini')
    return lang[os.getenv('LANGUAGE')]