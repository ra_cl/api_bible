import sqlite3
from lang import lang

class sql:
    def consulta(sql) -> str:
        error = False
        lng = lang()
        s = sqlite3.connect('data')
        c = s.cursor()
        c = c.execute(sql)
        r = c.fetchall()
        txt = ''
        if r == []:
            txt = lng['no_found']
            error = True
        else:
            for t in r:
                if txt != '':
                    txt += "<br>"
                txt += t[0]
            txt = txt.replace('<CM>','')
            txt = txt.replace('<FR>','')
            txt = txt.replace('<Fr>','')
        s.close()
        return {'txt':txt,'error':error}
