from sql import sql
from lang import lang

class book:
    text = ''
    def __init__(self, **kwargs) -> None:
        if kwargs != {}:
            self.text = self.text(**kwargs)
        pass

    def text(self, **kwargs):     
        txt = ''
        qry = ''
        l=lang()
        error=False
        if 'b' in kwargs:
            if 'c' in kwargs:
                if 'v' in kwargs:
                    qry = f"SELECT Scripture FROM Bible WHERE Book={kwargs['b']} AND Chapter={kwargs['c']} AND Verse={kwargs['v']}"
                else:
                    qry = f"SELECT Scripture FROM Bible WHERE Book={kwargs['b']} AND Chapter={kwargs['c']}"
        if qry=='':
            error=True
            txt = l['no_cap']
        else:
            r = sql.consulta(qry)
            txt = r['txt']
            error = r['error']
        return {'txt':txt, 'error':error}

    def __str__(self) -> str:
        return self.text['txt']

    def ok(self) -> bool:
        return not self.text['error']
