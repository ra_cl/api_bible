from book import book
from flask import Flask, jsonify
from lang import lang
import os


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )
    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.get('/')
    def index():
        lng = lang()
        use = lng['use']
        return jsonify({'error':True, 'text':use}), 404

    @app.get('/book/<int:bk>')
    def get_book(bk):
        b = book(b=bk)
        txt = f"{b}"
        err = not b.ok()
        status = 404 if err else 200
        return jsonify({'text':txt, 'error':err}), status

    @app.get('/book/<int:bk>/chapter/<int:ch>')
    def get_chapter(bk, ch):
        chap = book(b=bk, c=ch)
        txt = f"{chap}"
        err = not chap.ok()
        status = 404 if err else 200
        return jsonify({'text':txt, 'error':err}), status

    @app.get('/book/<int:bk>/chapter/<int:ch>/verse/<int:vr>')
    def get_verse(bk, ch, vr):
        ver = book(b=bk, c=ch, v=vr)
        txt= f"{ver}"
        err= not ver.ok()
        status = 404 if err else 200
        return jsonify({'text':txt, 'error':err}), status

    @app.errorhandler(404)
    def page_not_found(e):
        lng = lang('es')
        use = lng['use']
        return jsonify({'text':str(e)+"<br>\n"+use, 'error':True}), 404
    
    return app