# BIBLE API
## Objetivo
Entregar versos de la Biblia en formato json de salida. 

En caso de versos fuera de rango o de llamadas incorrectas retorna 404 con error en json.

En caso de ser correcto retorna un 200 con json y error en false.

## Instalación
En debian:
```bash
apt update
apt install git python3 python3-pip
git clone git@gitlab.com:ra_cl/api_bible.git
cd api_bible
pip install -r requeriments
```
## Entorno
Es necesario definir la variable de entorno **LANGUAGE** que definirá en que idioma se retornan los mensajes, los idiomas que actualmente se encuentran definidos son **es** para español y **en** para inglés.

| ENV VAR | Description | value |
| :---| :---| :---|
| LANGUAGE | error message language | **en** (english) <br>**es** (spanish)| 


## Puesta en marcha
Para ejecutar el servidor, después de configurar las variables de entorno, debe ejecutar 
```bash
waitress-serve --listen=*:8080 --no-ipv6 --call  'api:create_app'
```
## Test del entorno
Para probar que todo está funcionando ejecute:

Para obtener Juan 3:16
```bash
curl -i -L http://URL:8080/book/43/chapter/3/verse/16
```

Para obtener el Salmo 23
```bash
curl -i -L http://URL:8080/book/19/chapter/23
```
